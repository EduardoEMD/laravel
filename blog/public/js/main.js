
$(function(){

     $(document).on('click','.notify',function(){

            $(this).attr('data-id');
        })

        $(document).on('click','.notify-send',function(){
            
            var data = {};

            data["_token"] = $("input[name='_token']").val();
            data.msg = $('.msg_to_chef').val();
            data.id = $('.bgr_hidden').attr('data-id')
            data.quests = [];
            $('.quest_ans').each(function(){
                
                if( !jQuery.isEmptyObject($(this).val()) ){
                    data.quests.push($(this).val());
                }
            })

            $.ajax({
                type:"post",
                url:"./notify-send",
                data:data,
                success:function(r){
                    $('#success_msg').html("Yout Advertisement has been added successfully").show(300); 
                    $('.notify-send').detach();
                }
            })

        })
    $('#add_quest').click(function(){
            $('#questions').append('<label>Type question</label>',"<input class='hayt_quest form-control'>")

    })
        $('#submit').click(function(e){
            e.preventDefault();
            var data = new FormData(document.getElementById('form_data'));
            data["_token"] = $("input[name='_token']").val();
            data.title = $('input[name="title"]').val();
            data.text = $('textarea[name="text"]').val();
            var arr = [];

            $('.hayt_quest').each(function(){

                if( !jQuery.isEmptyObject($(this).val()) ){
                    arr.push($(this).val());
                }
            })
            data.append('quests',JSON.stringify(arr));

            $.ajax({
                
                type:"post",
                url:"./add",
                data:data,
                cache: false,
                contentType: false,
                processData: false,
                success:function(r){
                    r = JSON.parse(r)
                    if( r == 1) $('#success_msg').html("Yout Advertisement has been added successfully").show(300); $('#submit').off();
                                       
                    if(r.length > 0){
                        var text = ""
                        r.forEach(function(item){
                            text+=item+"<br>"
                        })

                        $('#error_msg').html(text).show(300)
                    }
                }
            })
        })

        $(document).on('click','.btn-bluer',function(){

            $('#must_show_loading').addClass('loader');

            var data = {};
            data['_token'] = $('input[name="_token"]').val();
            data.id = $(this).attr('data-id');
            var _this = $(this)
            $.ajax({
                type:'post',
                url:'./deleteProposal',
                data:data,
                success:function(r){
                        _this.closest('tr').fadeOut(1000)
                        $('#success_msg').html('Post has been successfully deleted').show(300);
                        $('#must_show_loading').removeClass('loader')
                        setTimeout(function(){
                             $('#success_msg').slideUp()
                        },2000)

                }
            })
        })

        $('.notif_acc').click(function(){
            $('#must_show_loading').addClass('loader')

            var data = {}
            data['_token'] = $('input[name="_token"]').val();
            data.msg = $(this).parent().find('.msg_back').val();
            data.id = $(this).data('id');

            $.ajax({
                url:'./req_acc',
                type:'post',
                data:data,
                success:function(r){
                    console.log(r)
                    $('#must_show_loading').removeClass('loader')
                    $('#success_msg').html('Feedback has been sent').show(300);
                    setTimeout(function(){
                             $('#success_msg').slideUp()
                    },1500)

                }

            })

        })
        $('.notif_dec').click(function(){
            $('#must_show_loading').addClass('loader')

            var data = {}
            data['_token'] = $('input[name="_token"]').val();
            data.msg = $(this).parent().find('.msg_back').val();
            data.id = $(this).data('id');

            $.ajax({
                url:'./req_decl',
                type:'post',
                data:data,
                success:function(r){
                    console.log(r)
                    $('#must_show_loading').removeClass('loader')
                    $('#success_msg').html('Feedback has been sent').show(300);
                    setTimeout(function(){
                        $('#success_msg').slideUp()
                    },1500)

                }

            })

        })
        


    })