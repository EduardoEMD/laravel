<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('insert',"HomeController@newContact");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('test','JobController@test');

Route::get('newAdvertisement','HomeController@newAdvertisement')->middleware('agemid');

Route::get('proposals', "HomeController@showAll");

Route::post('/add','JobController@insert');

Route::get('notify/{id}','JobController@notify_show');

Route::post('notify/notify-send',"JobController@notify_send");

Route::get('requests','JobController@requests_show')->middleware('auth');

Route::get('myProposals','JobController@myProposals')->middleware('auth');

Route::get('myFeedbacks','JobController@myFeedbacks')->middleware('auth');

Route::post('deleteProposal',"JobController@delete_post");

Route::post('req_acc',"JobController@req_acc");

Route::post('req_decl',"JobController@req_decl");



