@extends('layouts.app')

@section('content')

		{{ csrf_field() }}

	<div  id="must_show_loading"></div>
	<div class="container">
		<div class="col-md-8 col-md-offset-2">
			<p id="success_msg" class="alert alert-success" style="display: none;"></p>
			<table class="table-bordered">
				<tr>
					<th>id</th>
					<th>title</th>
					<th>created at</th>
					<th>delete</th>
				</tr>
				@foreach ($proposals as $p)
					<tr>
						<td>{{ $p->id }}</td>
						<td>{{ $p->title }}</td>
						<td>{{ $p->created_at }}</td>
						<td><button class="btn btn-bluer" data-id="{{ $p->id }}">Delete</button></td>
					</tr>
				@endforeach
			</table>
		</div>	
	</div>	



@endsection