@extends('layouts.app')

@section('content')
	<div class="container">
    	<div class="row">
        	<div class="col-md-6 col-md-offset-2">
        			<form enctype="multiple/form-data" id="form_data">
					{{ csrf_field() }}
					
					<div class="flash-message">
						<p id="error_msg" class="alert alert-danger" style="display: none;"></p>
						<p id="success_msg" class="alert alert-success" style="display: none;"></p>
					</div> 

					<label>Title</label><br>
					<input type="text" class="form-control" name="title"><br>
					
					<label>Description</label><br>
					<textarea type="text" class="form-control" name="text" cols="20" rows="5"></textarea><br>
					<label class="btn btn-default btn-file">
    				Choose poster for proposal <input type="file" name="image" hidden >
					</label><br><br>
					<input id="add_quest" type="button" value="Add question" class="btn btn-danger"><br>
					<div id="questions"></div><br>
					<input type="submit" id="submit" value="save" class="btn btn-success">
					</form>
			</div>
		</div>
	</div>

@endsection
