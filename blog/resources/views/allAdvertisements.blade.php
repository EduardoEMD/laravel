@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-md-8 col-md-offset-2">
			@foreach ($hayt as $h)
				<div class="row hayt">
					<h1 style="color: #FF5722;">{{ $h['title'] }}<small style="font-size: 45%;"> by : {{ $h->users->name }}</small></h1>
					
					@if ( $h->photo != "" )
						<img src="uploads/{{ $h->photo }}" width="350" height="200" class="img-thumbnail">
					@else
						<img src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSEcH6Dyx9_Ag4klf9QLlm8qeEkUBbJwOGFMkAXRvwrcghig09B" width="350" height="200" class="img-thumbnail">
					@endif
					<span class="desc"> {{ $h['text'] }} </span>
					<a href="./notify/{{ $h['id']}}" ><button class="btn btn-info notify" data-id="{{ $h['id'] }}">Դիմել</button></a>
				</div>

			@endforeach
	</div>
	</div>
@endsection
