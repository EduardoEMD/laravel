@extends('layouts.app')


@section('content')

	<div class="container">
		<div class="col-md-8 col-md-offset-2">
			@foreach($feedback as $f)
				<div class="hayt">
					@if ($f->result == "1")
						<h2 style="color: firebrick">Agreed</h2>
					@else
						<h2 style="color: lightgreen">Declined</h2>
					@endif
					<h3>sent by user :{{ $f->users->name }}</h3>
					<p>{{ $f->message }}</p>
				</div>

			@endforeach
		</div>
	</div>


@endsection