@extends('layouts.app')


@section('content')

	<div  id="must_show_loading"></div>
	<p id="success_msg" class="alert alert-success" style="display: none;"></p>

	
	<div class="container">
		<div class="col-md-8 col-md-offset-2">
			@foreach($requests as $r)
				{{ csrf_field() }}
				<div class="hayt">
					<h3>sent by user :{{ $r->users->name }}</h3>
					<h3>sent at :{{ $r->created_at }}</h3><br>
				
					<?php $answers = json_decode($r->answers);?>
					@foreach ($answers as $a)
							<label style="color:#A7D61F">Answer</label>
							<h2>{{ $a }} :</h2>
					@endforeach
					<label> Message: </label><br>
					<textarea class="msg_back" cols="30" rows="4"></textarea><br>
					<button class="btn btn-danger notif_dec" data-id="{{ $r->users->id }}">Decline</button>
					<button class="btn btn-success notif_acc" data-id="{{ $r->users->id }}">Accept</button>

				</div>

			@endforeach
		</div>
	</div>


@endsection