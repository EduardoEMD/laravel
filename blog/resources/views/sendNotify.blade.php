@extends('layouts.app')


@section('content')
	
	<p id="success_msg" class="alert alert-success" style="display: none;"></p>
	<div class="col-md-4 col-md-offset-2">
	
		{{ csrf_field() }}
					
		@foreach ($data as $d)
			<p style="display: none;" class="bgr_hidden" data-id="{{ $d->user_id}}"></p>
			<h1>{{ $d->title }}</h1>
			@if ( $d->photo != "" )
				<img src="../uploads/{{ $d->photo }}" width="350" height="200" class="img-thumbnail">
			@else
				<img src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSEcH6Dyx9_Ag4klf9QLlm8qeEkUBbJwOGFMkAXRvwrcghig09B" width="350" height="200" class="img-thumbnail">
			@endif
			<p class="descrip_send_notif">{{ $d->text }}</p>
			<p><b>Created at :</b> {{ $d->created_at }}</p>
			<p><b>Updated at :</b> {{ $d->updated_at }}</p>
			<textarea class="msg_to_chef form-control" rows="6" placeholder="Type message . . . "></textarea><br>
			@foreach ($d->questions as $q)
				<label>{{ $q }} :</label>
				<input type="text" class="form-control quest_ans">
			@endforeach
		
		@endforeach
		<br>
		<input type="button" class="btn btn-success notify-send" value="send">
		
	</div>
@endsection

