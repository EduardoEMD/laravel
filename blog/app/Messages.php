<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
   	protected  $table = "Messages";

	public $timestamps = true;

	public function users(){

		return $this->belongsTo('App\User','user2_id','id');
	}
}
