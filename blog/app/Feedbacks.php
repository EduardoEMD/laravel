<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedbacks extends Model
{
   	protected $table = "Feedbacks";

	public $timestamps = true;

	public function users(){

		return $this->hasOne('App\User','id','user2_id');
	}
}
