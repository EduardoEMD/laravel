<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Haytararutyun extends Model
{
   	protected  $table = "Haytararutyun";

	public $timestamps = true;


	public function users(){

		return $this->hasOne('App\User','id','user_id');
	}
}
