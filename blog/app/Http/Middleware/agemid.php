<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;


use Closure;

class agemid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!empty(Auth::user()) && Auth::user()->age >= 18 ) {

            return $next($request);
        }else{

            return Redirect::to('/')->withErrors('You are too young');
        } 
    }

}
