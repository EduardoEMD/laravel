<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Haytararutyun;
use App\Messages;
use App\User;
use App\Feedbacks;
use Validator;

class JobController extends Controller
{

	public function insert(Request $r){

		$title = $r->input('title');
		$text = $r->input('text');
		$quests = $r->input('quests');
		$id = Auth::user()->id;
	
		$validation = Validator::make(
			array(
				'title' => $title,
				'text' => $text
				),
			array(
				'title' => 'required|min:4|max:255',
				'text' => 'required|max:255'
				),
			array(
				'required' => ":attribute դաշտը պարտադիր պետք է լրացվի",
				'min' => ":attribute դաշտի երկարությունը չպետք է լինի ավելի փոքր քան 4",
				'max' => ":attribute դաշտի երկարությունը չպետք է գերազանցի 255"

				)
			);
		
		if($validation->fails()){

			print json_encode($validation->errors()->all());

		}else{
			
			$photo_name = "";

			if( $r->hasFile('image') ) {
				$file = $r->file('image');
	
				$test_array = explode('.', $file->getClientOriginalName());
				$path_name = $test_array[count($test_array)-1];
				$photo_name = uniqid().".".$path_name;
	
				$destinationPath = public_path().'\uploads';
	
				$file->move($destinationPath, $photo_name);

    		}
			
			$x = new Haytararutyun();

			$x->title = $title;
    		$x->text = $text;
    		$x->questions = $quests;
			$x->user_id = $id;
			$x->photo = $photo_name;
    		$x->save();

    		session()->flash('alert-success', 'The Advertisement was successful added!');

    		print 1;
   		}

	}

	public function notify_show($id){

		$x = new Haytararutyun();

		$data = $x->where('id', $id)->get();

		foreach ($data as $d) {
			$d->questions = json_decode($d->questions);
		}
		return view('sendNotify')->with('data',$data);

	}

	public function notify_send(Request $r){

		$id = Auth::user()->id;
		$message = $r->input('msg');
		$quests = $r->input('quests')==null? json_encode(array()):json_encode($r->input('quests'));
		$user2_id = intval($r->input('id'));

		$x = new Messages();

		$x->user1_id = $id;
		$x->user2_id = $user2_id;
		$x->text = $message;
		$x->answers = $quests;

		$x->save();
	


	}

	public function requests_show(){

		$id = Auth::user()->id;

		$x = new Messages();
		$requests = $x->where('user2_id', $id)->get();

		return view('allrequests')->with('requests',$requests);

	}

	public function myProposals(){

		$id = Auth::user()->id;

		$proposals = Haytararutyun::where('user_id',$id)->get();

		return view('myProposals')->with('proposals',$proposals);
	}

	public function delete_post(Request $r){

		$id = $r->input('id');

		Haytararutyun::where('id',$id)->delete();
		

	}

	public function req_acc(Request $r){

		
		$msg = $r->input('msg') == null ? "":$r->input('msg');
		$id = Auth::user()->id;

		$x = new feedbacks();

		$x->message = $msg;
		$x->result = '1';
		$x->user1_id = $id;
		$x->user2_id = $r->input('id');

		$x->save();
		Messages::where('user1_id',$r->input('id'))
				->where('user2_id',$id)
				->delete();



	}
	public function req_decl(Request $r){

		
		$msg = $r->input('msg') == null ? "":$r->input('msg');
		$id = Auth::user()->id;
		
		$x = new Feedbacks();

		$x->message = $msg;
		$x->result = '0';
		$x->user1_id = $id;
		$x->user2_id = $r->input('id');

		$x->save();
		Messages::where('user1_id',$r->input('id'))
				->where('user2_id',$id)
				->delete();



	}
	public function myFeedbacks(){

		$id = Auth::user()->id;
		
		$data = Feedbacks::where('user2_id',$id)->get();

		return view('feedbacks')->with('feedback',$data);
	}


}
